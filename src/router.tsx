import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
  useHistory,
  Prompt,
} from "react-router-dom";

export default function BasicExample() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <OldSchoolMenuLink
              activeOnlyWhenExact={true}
              to="/"
              label="Home" />
          </li>
          <li>
            <OldSchoolMenuLink
              activeOnlyWhenExact={true}
              to="/about"
              label="About" />
          </li>
          <li>
            <OldSchoolMenuLink
              activeOnlyWhenExact={true}
              to="/dashboard/1/demo"
              label="get param on url" />
          </li>
          <li>
            <OldSchoolMenuLink
              activeOnlyWhenExact={true}
              to="/nesting"
              label="go to nesting new lesson" />
          </li>
        </ul>

        <hr />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/dashboard/:id/demo" children={<Dashboard />} />
          <Route path="/nesting" children={<Nesting />} />
          <Route path="*">
            <NoMatch />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

// You can think of these components as "pages"
// in your app.

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

function About() {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}

function Dashboard() {
  const params:any = useParams();
  return (
    <div>
      <h2>Dashboard</h2>
      <h3>{ params.id }</h3>
    </div>
  );
}

// ================ || ========================

const Nesting = () => {
  const { path, url } = useRouteMatch();
  return (
    <div>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to={`${url}/auth`}>auth</Link>
        </li>
        <li>
          <Link to={`${url}/history`}>active history</Link>
        </li>
        <li>
          <Link to={`${url}/location`}>active location</Link>
        </li>
        <li>
          <Link to={`${url}/private`}>active private</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}




function Topic() {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  const params: any = useParams();
  const isPropmt = true;
  const history = useHistory();
  const location = useLocation();
  const goToLocation = () => {
    // history.push('/nesting/location');
    history.replace(location.pathname);
  }
  if (params.topicId == 'history') {
    return (
      <div>
        <h3>{params.topicId}</h3>
        <Prompt
          when={isPropmt}
          message={loca => `Are you sure you want to go to ${loca.pathname}`}
        />
        <button onClick={goToLocation}>go to location</button>
        <div>
          <pre>
            {JSON.stringify(history)}
          </pre>
        </div>
      </div>
    )
  }
  if (params.topicId == 'location') {
    return (
      <div>
        <h3>{params.topicId}</h3>
        <div>
          <pre>
            {JSON.stringify(location)}
          </pre>
        </div>
      </div>
    )
  }
  if (params.topicId == 'auth') {
    return (
      <div>
        <h3>{params.topicId}</h3>
        <div>
          <pre>
            {JSON.stringify(location)}
          </pre>
        </div>
      </div>
    )
  }
  return (
    <div>
      <NoMatch />
    </div>
  );
}

function OldSchoolMenuLink(inFo: any) {
  let match = useRouteMatch({
    path: inFo.to,
    exact: inFo.activeOnlyWhenExact
  });

  return (
    <div className={match ? "active" : ""}>
      {match && "> "}
      <Link to={inFo.to}>{inFo.label}</Link>
    </div>
  );
}