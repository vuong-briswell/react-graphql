/// <reference types="node" />
/// <reference types="react" />
/// <reference types="react-dom" />

declare namespace NodeJS {
  interface ProcessEnv {
    readonly NODE_ENV: 'development' | 'production' | 'test';
    readonly PUBLIC_URL: string;
  }
}

declare module '*.avif' {
  const src: string;
  export default src;
}

declare module '*.bmp' {
  const src: string;
  export default src;
}

declare module '*.gif' {
  const src: string;
  export default src;
}

declare module '*.jpg' {
  const src: string;
  export default src;
}

declare module '*.jpeg' {
  const src: string;
  export default src;
}

declare module '*.png' {
  const src: string;
  export default src;
}

declare module '*.webp' {
    const src: string;
    export default src;
}

declare module '*.svg' {
  import * as React from 'react';

  export const ReactComponent: React.FunctionComponent<React.SVGProps<
    SVGSVGElement
  > & { title?: string }>;

  const src: string;
  export default src;
}

declare module '*.module.css' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.module.scss' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.module.sass' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare namespace JSX {
  interface IntrinsicElements {
    Wellcome: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    formEvent: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    h2Tag: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement, { foo: any }>;
    UseLazyQueryHandler: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    HookLearning: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    TestRender: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    User: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
  }
}