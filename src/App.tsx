import { Component, useEffect, useState, useMemo, useCallback, useContext, useRef, useReducer } from 'react';
import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery  } from '@apollo/client';
import './App.css';
import * as redux from 'redux';

const counter = (state = 0, action: any) => {
  console.log(action);
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
  }
  return state;
};

const store = redux.createStore(counter);



const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

const themeContent = React.createContext(themes.dark);

interface IEvent {
  _id: string,
  title: string,
  description: string,
  price: string,
  date: string
}



const graphql = gql`
  query {
    events {
      _id,
      title,
      description,
      price,
      date
    }
  }
`;

const GET_TODO_LIST = gql`
  mutation ($title: String, $description: String, $price: String, $date: String) {
    createEvent(eventInput: {title: $title, description: $description, price: $price, date: $date}) {
      title,
      description,
      price,
      date
    }
  }
  `;

const GET_CONDITION_TODO = gql`
  query ($index: String, $value: String){
    getEvent(data: {index: $index, value: $value}) {
      _id,
      title,
      description,
      price,
      date
    }
  }
`
function UseLazyQueryHandler() {
  const [index, setIndex] = useState('_id');
  const [value, setValue] = useState('');
  const [getToDo, { loading, data }] = useLazyQuery(GET_CONDITION_TODO);

  if (loading) {
    return (
      <p>Loading lazy query ...</p>
    )
  }

  const handleChangeSelect = (e: any) => {
    setIndex(e.target.value);
  }

  const handleChangeValue = (e: any) => {
    setValue(e.target.value);
  }

  const handlerOnClick = (e: any) => {
    e.preventDefault();
    getToDo({
      variables: {
        index,
        value
      }
    });
  }
  
  return (
    <div>
      <select name="" id="" onChange={handleChangeSelect} value={index}>
        <option value="_id">_id</option>
        <option value="title">title</option>
        <option value="description">description</option>
        <option value="price">price</option>
        <option value="date">date</option>
      </select>
      <input type="text" onChange={handleChangeValue} value={value}/>
      <button onClick={handlerOnClick}>NÀ NÍ</button>
      <div className="show-event">
        {
          JSON.stringify(data)
        }
      </div>
    </div>
  )
}

function Wellcome() {
  const { loading, data, error , refetch, networkStatus} = useQuery(graphql, {
    pollInterval: 1000000,
    ssr: false
    // query: graphql
  });
  if (loading) return <p>loading ...</p>;
  if (error) return <p>{error}</p>;
  
  const id = data.events.map((event: IEvent) => {
    return <li key={event._id}>{'id: ' + event._id}</li>
  });
  const title = data.events.map((event: IEvent) => {
    return <li key={event._id}>{'title: ' + event.title}</li>
  });
  const description = data.events.map((event: IEvent) => {
    return <li key={event._id}>{'description: ' + event.description}</li>
  });
  const price = data.events.map((event: IEvent) => {
    return <li key={event._id}>{'price: ' + event.price}</li>
  });
  const date = data.events.map((event: IEvent) => {
    return <li key={event._id}>{'date: ' + event.date}</li>
  });
  return (
    <div>
      <ul className="id">
        {id}
      </ul>
      <ul className="id">
        {title}
      </ul>
      <ul className="id">
        {description}
      </ul>
      <ul className="id">
        {price}
      </ul>
      <ul>
        {date}
      </ul>
    </div>
  );
}

interface TypesProps {
  titleInput?: any,
}

interface TypesState {
  title: string,
  description?: string,
  date?: string,
  price?: string
}

const AppFunc = () => {
  let [title, setTitle] = useState('');
  let [description, setDescription] = useState('');
  let [price, setPrice] = useState('');
  let [date, setDate] = useState('');
  const [createEvent, { error, data, loading, called, client }] = useMutation(GET_TODO_LIST, {
    update: () => {
      
    }, // here is a function to update the cache after a mutation occurs,
    ignoreResults: false, // if true data property will not update with the mutation result.
    optimisticResponse: {
      __typename: 'Mutation',
      likePost: {
        __typename: "Post",
        title,
        description,
        price,
        date
      }
    },
    refetchQueries: result => [{
      query: graphql,
    }],
    awaitRefetchQueries: true,// đồng bộ/async 
    onCompleted: (voids) => { // gọi khi hoàn thành truy vấn // will be called when complete
      console.log(voids); 
    },
    onError: (error) => { // a callback will be called . when have errors
      console.log(error);
    },
    context: ['one', 'two', 'three'],// i don't know exactly. but i readed it and be understanded it using for  shared  context between your component and your network interface. and it useful for setting headers from props or sending information to the request 
    // client: 
  });
  
  const handleChangeTitle = (e: any) => {
    setTitle(e.target.value);
  }
  const handleChangeDescription = (e: any) => {
    setDescription(e.target.value);
  }
  const handleChangeDate = (e: any) => {
    setDate(e.target.value);
  }
  const handleChangePrice = (e: any) => {
    setPrice(e.target.value);
  }
  const submitEvent = (e: any) => {
    e.preventDefault();
    const dataEvent = {
      title,
      description,
      price,
      date
    }
    
    createEvent({
      variables: {
        ...dataEvent,
      }
    });
  }
  return (
    <form>
      title:       <input type="text" value={title} onChange={handleChangeTitle} /><br />
      description: <input type="text" value={description} onChange={handleChangeDescription} /><br />
      price:       <input type="text" value={price} onChange={handleChangePrice} /><br />
      date:        <input type="text" value={date} onChange={handleChangeDate} /><br />
      <button onClick={submitEvent}>submit !</button>
    </form>
  );
}
export default class App extends Component<TypesProps, TypesState> {
  constructor(props: any) {
    super(props);
    this.state = {
      title: '',
      description: '',
      date: '',
      price: ''
    }
  }
  render() {
    return (
      <div className="App">
        <Wellcome key="1" />
        <div className="div-form">
          <h1>Form create event</h1>
          <AppFunc />
        </div>
        <UseLazyQueryHandler />
        <HookLearning foo="asd"/>
      </div>
    );
  }
}

// function 

function init() {
  return { count: 10 };
}

const HookLearning = (event: any) => {
  const [click, setClick] = useState(0);
  const themes = useContext(themeContent);
  const inputEl = useRef(null);
  const [state, dispatch] = useReducer((state: any, action: any) => {
    store.dispatch({ type: 'INCREMENT' })
    switch(action.type) {
      case 'increment':
        return { count: state.count + 1};
      case 'decrement':
        return { count: state.count - 1 };
    default:
    throw new Error();
    }
  }, {}, () => {
    return { count: 10 }
  });

  useEffect(() => {
    console.log('=============almost same as componentWillUnmount=================');
    return () => {
      console.log('clear component');
    }
  }, [click]);

  const useMemoDemo = useMemo(() => {
    console.log('useMeno: ', event);
  }, [event.foo]);

  const toggleChecked = useCallback(() => {
    
  }, [click]);
  return (
    <div>
      <p>hook's react</p>
       Count: {state.count}
      <button onClick={() => dispatch({ type: 'decrement' })}>-</button>
      <button onClick={() => dispatch({ type: 'increment' })}>+</button>
      <button ref={inputEl} onClick={toggleChecked} style={themes}>Click me</button>
      { click }
    </div>
  );
}

export const TestRender = (props: any) => {
  return (
    <div>
      <p className="demo-test">demo test renere</p>
      {props.id}
    </div>
  )
}


export function useCounter() {
  const [count, setCount] = useState(0)

  const increment = useCallback(() => setCount((x) => x + 1), [])

  return { count, increment }
}

export const useCouterStepOne = (init = 0) => {
  const [count, setCount] = useState(init);
  const increment = useCallback(() => setCount((x) => x + 1), []);
  return { count, increment };
}

export const useCounterStepTwo = (initialValue = 0) => {
  const [count, setCount] = useState(initialValue)
  const increment = useCallback(() => setCount((x) => x + 1), [])
  const reset = useCallback(() => setCount(initialValue), [initialValue])
  return { count, increment, reset }
}