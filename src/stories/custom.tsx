import React from 'react';
import './button.css';

export interface ButtonProps {
    /**
     * Is this the principal call to action on the page?
     */
    primary?: boolean;
    /**
     * What background color to use
     */
    backgroundColor?: string;
    /**
     * How large should the button be?
     */
    size?: 'small' | 'medium' | 'large';
    /**
     * Button contents
     */
    label: string;
    /**
     * Optional click handler
     */
    onClick?: () => void;
}

export function useCounter(initialValue = 0) {
    const [count, setCount] = React.useState(initialValue)
    const increment = React.useCallback(() => setCount((x) => x + 1), [])
    const reset = React.useCallback(() => setCount(initialValue), [initialValue])
    return { count, increment, reset }
}