import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { storiesOf } from "@storybook/react";
import { useCounter, ButtonProps } from './custom';
import styled from "styled-components";

const Container = styled.div`
  font-size: 50px
`;

storiesOf('Example/ButtonCustom', module)
    .add('basic', () => {
        return <Container><div>内容内容内容</div></Container>;
    }).add('no sub', () => {
        return <p> hoho </p>;
})