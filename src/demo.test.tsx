import React from 'react'
import ReactDom, { unmountComponentAtNode  } from 'react-dom'
import { TestRender, useCounter, useCouterStepOne, useCounterStepTwo } from './App';
import TestRenderer from 'react-test-renderer';
import ReactTestUtils, { act } from 'react-dom/test-utils';
import * as reactHooksTest from '@testing-library/react-hooks';
// // describe('ProductHeader Component', () => {

// //     it('has an h2 tag', () => {
// //         //Test here
// //     });

// //     it('is wrapped inside a title class', () => {
// //         //Test here
// //     })
// // })

// // test('adds 1 + 2 to equal 3', () => {
// //     expect(sum(1, 2)).toBe(3);
// // });

// // test('object assignment', () => {
// //     expect(obje()).toEqual({ one: 1, two: 2 });
// // });

// test('adding positive numbers is not zero', () => {
//     for (let a = 1; a < 10; a++) {
//         for (let b = 1; b < 10; b++) {
//             expect(a + b).not.toBe(0);
//         }
//     }
// });
// // systax basic test
// test('null', () => {
//     const n = null;
//     expect(n).toBeNull();
//     expect(n).toBeDefined();
//     expect(n).not.toBeUndefined();
//     expect(n).not.toBeTruthy();
//     expect(n).toBeFalsy();
//     // toBeNull matches only null
//     // toBeUndefined matches only undefined
//     // toBeDefined is the opposite of toBeUndefined
//     // toBeTruthy matches anything that an if statement treats as true
//     // toBeFalsy matches anything that an if statement treats as false
// });

// test('zero', () => {
//     const z = 0;
//     expect(z).not.toBeNull();
//     expect(z).toBeDefined();
//     expect(z).not.toBeUndefined();
//     expect(z).not.toBeTruthy();
//     expect(z).toBeFalsy();
// });
// // number 
// test('two plus two', () => {
//     const value = 2 + 2;
//     expect(value).toBeGreaterThan(3);
//     expect(value).toBeGreaterThanOrEqual(4);
//     expect(value).toBeLessThan(5);
//     expect(value).toBeLessThanOrEqual(4.5);

//     // toBe and toEqual are equivalent for numbers
//     expect(value).toBe(4);
//     expect(value).toEqual(4);
// });

// test('adding floating point numbers', () => {
//     const value = 0.1 + 0.2;
//     //expect(value).toBe(0.3); This won't work because of rounding error
//     expect(value).toBeCloseTo(0.3); // This works.
// });

// // string
// test('but there is a "stop" in Christoph', () => {
//     expect('Christoph').toMatch(/stop/);
// });

// // array
// test('the shopping list has milk on it', () => {
//     expect(arr()).toContain('milk');
//     expect(new Set(arr())).toContain('milk');
// });

// //Exceptions
// test('compiling android goes as expected', () => {
//     expect(() => exceptions()).toThrow();
//     expect(() => exceptions()).toThrow(Error);

//     // You can also use the exact error message or a regexp
//     expect(() => exceptions()).toThrow('you are using the wrong JDK');
//     expect(() => exceptions()).toThrow(/JDK/);
// });


// // // Testing Asynchronous Code
// // beforeEach(() => {
// //     // console.log('befor each');
// //     // console.log(myMock.mock.instances);
// // })

// // afterEach(() => {
// //     // console.log('after each');
// // })

// // beforeAll(() => {
// //     // console.log('before All');
// // })

// // afterAll(() => {
// //     // console.log('after All');
// // })

// describe('outer', () => {
//     // console.log('describe outer-a');

//     describe('describe inner 1', () => {
//         // console.log('describe inner 1');
//         test('test 1', () => {
//             // console.log('test for describe inner 1');
//             expect(true).toEqual(true);
//         });
//     });

//     // console.log('describe outer-b');

//     test('test 1', () => {
//         // console.log('test for describe outer');
//         expect(true).toEqual(true);
//     });

//     describe('describe inner 2', () => {
//         // console.log('describe inner 2');
//         test('test for describe inner 2', () => {
//             // console.log('test for describe inner 2');
//             expect(false).toEqual(false);
//         });
//     });

//     // console.log('describe outer-c');
// });

// // how to run 

// // beforeAll(() => console.log('1 - beforeAll'));
// // afterAll(() => console.log('1 - afterAll'));
// // beforeEach(() => console.log('1 - beforeEach'));
// // afterEach(() => console.log('1 - afterEach'));
// // test('', () => console.log('1 - test'));
// // describe('Scoped / Nested block', () => {
// //     beforeAll(() => console.log('2 - beforeAll'));
// //     afterAll(() => console.log('2 - afterAll'));
// //     beforeEach(() => console.log('2 - beforeEach'));
// //     afterEach(() => console.log('2 - afterEach'));
// //     test('', () => console.log('2 - test'));
// // });
// // steps
// // ----------||-----------
// // 1 - beforeAll
// // 1 - beforeEach
// // 1 - test
// // 1 - afterEach
// // 2 - beforeAll
// // 1 - beforeEach
// // 2 - beforeEach
// // 2 - test
// // 2 - afterEach
// // 1 - afterEach
// // 2 - afterAll
// // 1 - afterAll

// // test.only('this will be the only test that runs', () => {
// //     expect(true).toBe(true); // => chỉ test này duy nhất còn tất cả test khác sẽ không chạy
// // });

// // test('this test will not run', () => {
// //     expect('A').toBe('A');
// // });

// describe('custom', () => {
//     it('has an h2 tag', () => {
//         expect('a').toEqual('a');
//     })
//     it('has an h2 tag next', () => {
//         expect('a').toEqual('a');
//     })
//     it('should fire a alert', () => {
//         const alert = jest.fn();
//         document.body.innerHTML =
//             '<div>' +
//             '  <span id="username" />' +
//             '  <button id="button" />' +
//             '</div>';
//         // console.log(alert);
//     })
// });

// describe('demo', () => {
//     it('h2Tag has an h2 tag', () => {
//         const testRenderer = TestRenderer.create(
//             <h2Tag />
//         );

//         // console.log(testRenderer.toJSON());
//     });
//     it('inside have class title', () => {
//         const testRenderer = TestRenderer.create(<h2Tag />);
//         const testInstance = testRenderer.root;
//         // console.log(testInstance.findAllByProps({ className: 'title' }));
//         console.log(testInstance.findByType(h2Tag).props.foo);
        
//         // expect(testInstance.findByProps({ className: "title" })).toEqual(['title']);
//     });
// })

// describe('test render', () => {
//     it(' is tag p', () => {
//         // const div = document.createElement('div');
//         // act(() => {
//         //     ReactDom.render(<TestRender />, div);
//         // });
        
//         // expect(div.textContent).toBe('demo test renere')
//     });
//     it('test module render', () => {
//         let root: any;
//         TestRenderer.act(() => {
//             root = TestRenderer.create(<TestRender foo={ 123 } />);
//         });
//         console.log(root!.toJson());
        
//     });
// })

describe('test hook', () => {
    it('hook useCouter', () => {
        const { result } = reactHooksTest.renderHook(() => useCounter());
        reactHooksTest.act(() => {
            result.current.increment();
        });
        expect(result.current.count).toBe(1);
    })
    it('hook next 1', () => {
        const { result } = reactHooksTest.renderHook(() => useCouterStepOne(9000));
        reactHooksTest.act(() => {
            result.current.increment();
        });
        expect(result.current.count).toBe(9001)
    })
    it('hook next 2', () => {
        let initialValue = 0;
        const { result, rerender } = reactHooksTest.renderHook(() => useCounterStepTwo(initialValue));
        initialValue = 10;
        rerender();
        reactHooksTest.act(() => {
            result.current.reset();
        })
        console.log(result.current.count);
        
        expect(result.current.count).toBe(10)
    })
    it('hook next 3', () => {
        let id = 'first';
        const { rerender } = reactHooksTest.renderHook(() => {
            React.useEffect(() => {
                console.log('start id');
                
                return () => {
                    console.log('stop id');
                    
                }       
            }, [id])
        })
        // id = 'second';
        console.log(id);
        rerender();
    })
})